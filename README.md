``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run watch

# build for production with minification
npm run prod